from dotenv import load_dotenv
import openai
import os
from pathlib import Path

openai.api_key= os.getenv("OPENAI_API_KEY")



def gen_new_text(filepath:str, n:int, append:str="\n\n**Headline 7**\n", summarize:bool=False, temp:float=0.9, max_tokens:int=350, top_p:int=1, frequency_penalty:float=0.8,presence_penalty:float=0.6, stops:str="**Headline 8**"):
    '''
    uses gpt3 model to generate new text based on input and save to file. 
    :param filepath: str describing path to file
    :param n: int for repetition
    :param append: str to append to text for continuing generation prompt 
    :param summarize: bool adds tl;dr tag for summarization (note: doesn't always work well)
    :..rest are all params for openai model

    '''
    txt = Path(filepath).read_text() 
    if summarize:
        txt = txt + "\ntl;dr: "
    else:
        txt = txt + append   
    for i in range(n): 
        newtxt  = openai.Completion.create(
                    prompt = txt,
                    engine = "davinci",
                    temperature = temp,
                    max_tokens = max_tokens,
                    top_p = top_p, 
                    frequency_penalty = frequency_penalty,
                    presence_penalty = presence_penalty,
                    stop = stops 
                    )
        newtxt = newtxt.choices[0].text.strip()

        with open(f"sum{i+1}.txt", "w") as text_file:
            text_file.write(newtxt)




if __name__ == "__main__":
    gen_new_text("data.txt", 2, summarize=True)



