import os
import config as conf
import praw
from praw.models import MoreComments
from profanity import profanity

# TODO: add openai option
# TODO: filter profanity post too? 
reddit = praw.Reddit(client_id= conf.CLIENT_ID, client_secret=conf.CLIENT_SECRET, user_agent=conf.USER_AGENT)

def get_data_reddit(subreddit, datform, num_posts, save_dir): 
    ''' Scrape subreddit  and get training input/target (post/comment) pairs; saves file default in current directory data folder 
    subreddit: name of subreddit to scrape
    datform: either 'parlai' or 'openai' for compatable respective .txt or .jsonl files **CURRENTLY ONLY PARLAI OPTION
    num_posts: max number of posts to look through
    save_dir: directory to save data file 
    '''
    dirname = os.getcwd()+ save_dir 
    # create data dir if needed
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    if datform == 'parlai':
        datfile = 'dat.txt'
    elif datform == 'openai':
        datfile = 'dat.jsonl'
    else: 
        raise ValueError("Please pick 'parlai' or 'openai' for datform")

    f = open(dirname + datfile, 'w')
    hot_posts = reddit.subreddit(subreddit).hot(limit=num_posts)

    for post in hot_posts:
        if datform == 'parlai':
            input_val = "text:" + post.title

        print(post.title)
        n_submissions=0
        submission = reddit.submission(id=post.id)
        
        submission.comments.replace_more(limit=0) # replaces or removes the MoreComments (otherwise submissions w/ more comments will throw AttributeError)
        for top_lvl_comment in submission.comments:
            n_submissions+=1
            if submission.over_18 or profanity.contains_profanity(top_lvl_comment.body):
                continue
            target_val="labels:" + top_lvl_comment.body.replace("\n", " ")
            f.write(input_val + "\t" + target_val + "\t" + "episode_done:True")
        print("Num comments: {}".format(n_submissions))

    f.close()


if __name__ == "__main__": 
    get_data_reddit(subreddit=conf.subreddit, datform=conf.datform, num_posts=conf.num_posts, save_dir=conf.save_dir)

