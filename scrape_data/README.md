The script `src/scraper.py` gets subreddit data in the dialogue format for ParlAI model training and saves it in a .txt file, default in a `data` folder where you run the script.

First install the requirements with `pip install -r requirements.txt`, then edit the `src/config.py` file. 

To get the reddit api authentication info, follow the screenshots under 'Getting Started' from [this link](https://gilberttanner.com/blog/scraping-redditdata).


TODO: May add option to create data for other models. 

