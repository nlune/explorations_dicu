# Setup


## Install requirements 
```bash
pip install -r requirements.txt
```

## Terminal chat
Play around with chatting with transformer models in terminal see: `ai_talk/talkshow_helpers.py`


## Telegram deployment

### Channel
- You need to create a telegram channel
- get telegram api & hash after creating app from https://my.telegram.org/apps, 
- create a bot via Botfather, 
- add the bot as admin in the telegram channel, 
- run the script `ai_talk/bot_demo.py` and enter the API token for your bot


- command to run channel telegram bot in background and save output:
`nohup python -W ignore bot_demo.py > airtime.out &`
