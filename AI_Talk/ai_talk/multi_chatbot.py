from telethon import TelegramClient, events
from telethon import Button
import asyncio
from models.blenderbot import Blenderbot 
from models.dialogpt import DialoGPT
from models.openai_gpt3 import Davinci 
from transformers import BatchEncoding
import torch
import random
from datetime import datetime
import time
import pytz
import numpy as np
import logging
import os
from dotenv import load_dotenv
load_dotenv() 
from tinydb import TinyDB, Query, where

logging.basicConfig(level=logging.INFO)
# TODO: debug why /pass added to history, fix typing action, handle audio/emoji/gifs? 
# TODO: add /help 

class MultiBotChat:
    """
    This class allows users to interact with multple AI models in a conversation on a specific generated topic on Telegram. 
    """
    def __init__(self, session_user:str, api_id:int, api_hash:str, db_name:str='users.json'):
        """
        Initializes the MultiBotClient
        :param session_user: chosen username for TelegramClient
        :param api_id: Telegram's api_id acquired through my.telegram.org.
        :param api_hash: Telegram's api_hash.
        :param self.db_name: name of .json file for TinyDB
        """
        print("Initializing MultiBotClient...")
        
        # Create the client and connect
        self.client = TelegramClient(session_user, api_id, api_hash) 
        self.client.start()

        self.db = TinyDB(db_name)

        print("Initializing Agents...")
        self.blenderbot = Blenderbot()
        self.dialobot = DialoGPT(temperature=0.5, repetition_penalty=1.01)
        self.davinci = Davinci() 

        # max position embeddings
        self.blender_max_pos = 128
        self.dialo_max_pos = 128


        self.generate_conversation_instruction = "Give an interesting, philosophical, and provocative conversation starter about democracy, theatre, AI, or the future:\t"
        
    async def get_convo_topic(self): 
        topic  = self.davinci.generate_conversation_topic(self.generate_conversation_instruction)

        # make sure convo topic not blank or too short
        while len(topic) < 3:
            print('regenerating starter topic..')
            topic = self.davinci.generate_conversation_topic(self.generate_conversation_instruction)

        return topic

    async def tensor_to_hist(self, history, dialo=False):
        '''
        convert tensor to format for saving in database. defaults to blender
        '''
        if dialo:
            return history.cpu().numpy().tolist()
        else:
            new_history = {}
            for k in history: 
                new_history[k] = history[k].cpu().numpy().tolist() 
            return new_history 

    async def hist_to_tensor(self, history, dialo=False):
        '''
        converts history saved in db to tensor for tokenizer. defaults to blender
        '''
        if dialo:
            return torch.from_numpy(np.array(history))
        else:
            # convert history to original tensor form for tokenizer
            for k in history: 
                history[k] = torch.from_numpy(np.array(history[k]))
            return  BatchEncoding(history)

    async def update_blender_history(self, new_response, user_id, history_clip_buffer=20): 
        """
        save new user input to history in db  in text form, clip to len 128 if necessary. 
        save in text form instead of torch tensors, 
        max token len 128. 
        """
        user = self.db.search(Query().user_id==user_id)[0]
        retained_history_idx  = user['retained_history_idx_blender']
        history = user['blender_history']
        # convert history to original tensor form for tokenizer
        history = await self.hist_to_tensor(history)

        response = self.blenderbot.tokenizer([new_response + "\n"], return_tensors='pt') # input_ids and attention_mask dict
        for k in response:
            history[k] = torch.cat([history[k], response[k]], dim=-1)

        # clip history if necessary, and save new clipped history
        # TODO: could change this to be within the blenderbot class, w/ external history passed in
        if history["input_ids"].shape[-1] >= self.blender_max_pos:
            logging.info("clipping blender history...")
            excess = history["input_ids"].shape[-1] - self.blender_max_pos + history_clip_buffer 
            for k in history: 
                history_to_keep = history[k][:, :retained_history_idx]
                clipped_history = history[k][:, retained_history_idx + excess:]
                history[k] = torch.cat([history_to_keep, clipped_history], dim=-1) 
        # convert format for saving in db
        new_history = await self.tensor_to_hist(history)

        # update blender history in db
        self.db.update({'blender_history': new_history}, Query().user_id==user_id) 

    async def update_dialo_history(self, new_response, user_id, history_clip_buffer = 20):
        """
        save new user input to history in db  in text form, clip to len 128 if necessary. 
        save in text form instead of torch tensors, 
        max token len 128. 
        """
        user = self.db.search(Query().user_id==user_id)[0]
        retained_history_idx  = user['retained_history_idx_dialo']
        # convert history back to tensor form
        history = await self.hist_to_tensor(user['dialo_history'], dialo=True)

        response_ids = self.dialobot.tokenizer.encode( new_response + self.dialobot.tokenizer.eos_token, return_tensors='pt')
        new_history = torch.cat([history, response_ids], dim=-1)

        if new_history.shape[-1] >= self.dialo_max_pos:
            logging.info('clipping dialo history...')
            history_to_keep = new_history[:, : retained_history_idx]
            excess = new_history.shape[-1] - self.dialo_max_pos  + history_clip_buffer 
            clipped_history = new_history[:, retained_history_idx + excess:]

            # keep only initial history and most recent history
            new_history = torch.cat([history_to_keep, clipped_history], dim=-1)

        # convert tensor for saving in db
        new_history = new_history.cpu().numpy().tolist()

        # update dialo history in db
        self.db.update({'dialo_history': new_history}, Query().user_id==user_id) 

            

    async def user_in_db(self, user_id): 
        if self.db.search(Query().user_id==user_id):
            return True
        return False


    async def start_handler(self, event):
        '''
        Inform user of options, display keyboard yes no option to have conversation (consent)
        '''
        keyboard = [Button.inline('Yes!', b'yes'), Button.inline('Not now', b'no')]
        await event.reply('Would you like to have a conversation? \nYou can say /stop or /bye at any time to leave the conversation entirely, or /newtopic if you want to talk about something else. \nA topic will be generated after you click the yes button.', buttons=keyboard)


    async def yes_handler(self, event):
        '''
        handles yes button press from start_handler 
        '''
        user = await event.get_sender() 
        first_name = user.first_name
        user_id = user.id
        user_exists = await self.user_in_db(user_id)
        if not user_exists:
            await event.respond(f"Thanks for consenting, {first_name}! Here's your conversation topic (if you don't like it, just say /newtopic). If you forget the conversation topic later, say /reminder. Either Dialo or Blender will respond after your initial answer to the conversation prompt (be patient, they can sometimes be slow). If you're at a loss for words, say /pass to skip your turn.")

            topic  = await self.get_convo_topic()
            await event.respond(f"<b>{topic}</b>", parse_mode="html")

            retained_hist_idx_dialo, dialo_history = self.dialobot.set_initial_history(topic, return_history_idx=True)
            retained_hist_idx_blender, blender_history = self.blenderbot.set_initial_history(topic, return_history_idx=True)
            # convert history tokens to list for json
            dialo_history = dialo_history.cpu().numpy().tolist()
            # need to convert BatchEncoding obj to dict for json
            blender_hist = await self.tensor_to_hist(blender_history)

            # add user and topic hist to db
            self.db.insert({'user_id': user_id, 'firstName': first_name, 'topic': topic, 'blender_history':blender_hist, 'dialo_history': dialo_history, 'retained_history_idx_dialo': retained_hist_idx_dialo, 'retained_history_idx_blender': retained_hist_idx_blender})

        
            # delete below
            # for k in blender_hist: 
            #     blender_hist[k] = torch.from_numpy(np.array(blender_history[k]))

            # blender_hist = BatchEncoding(blender_hist)
            # blend_resp = self.blenderbot.get_response(input_ids_extern=blender_hist)
            # await event.respond(f"Blender: {blend_resp}")
            # await self.update_dialo_history(blend_resp, user_id)
            # await self.update_blender_history(blend_resp,user_id)

            # hist_tokens = np.array(dialo_history)
            # hist_tokens = torch.from_numpy(hist_tokens)
            # dialo_resp = self.dialobot.get_response(input_ids_extern=hist_tokens)
            # await event.respond(f"Dialo: {dialo_resp}")
            # await self.update_dialo_history(dialo_resp, user_id)
            # await self.update_blender_history(dialo_resp, user_id)

            # dialo_history = self.db.search(Query().user_id==user_id)[0]["dialo_history"]
            # hist_tokens = torch.from_numpy(np.array(dialo_history))
            # print("dialo hist: ",self.dialobot.tokenizer.decode(hist_tokens[:][0]))

            # blender_hist = self.db.search(Query().user_id==user_id)[0]["blender_history"]
            # for k in blender_hist: 
            #     blender_hist[k] = torch.from_numpy(np.array(blender_history[k]))

            # blender_hist = BatchEncoding(blender_hist)
            # print(f"blender hist: {self.blenderbot.tokenizer.decode(blender_hist['input_ids'][:][0], skip_special_tokens=True)}")

        else:
            await event.respond("You already consented")
        print(self.db.search(Query().user_id==user_id))
        

            
    async def no_handler(self,event):
        '''
        handle no button press from start_handler 
        '''
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists:
            await event.respond("To remove consent and leave the conversation, say /stop or /bye.")
        else:
            await event.respond('Alright.')

    async def response_handler(self,event):
        '''
        if user is in database, continue chat, otherwise inform of options
        '''
        user_id = event.sender_id
        user_msg = event.raw_text
        user_exists = await self.user_in_db(user_id)


        if not user_msg.startswith('/'):
            # case user already consented
            if user_exists:
                # update history w/ user message
                await self.update_blender_history(user_msg, user_id)
                await self.update_dialo_history(user_msg, user_id)
                # choose agent for response
                if "blender" in user_msg.lower():
                    agent_name = "Blender"
                    agent = self.blenderbot 
                elif "dialo" in user_msg.lower():
                    agent_name="Dialo"
                    agent = self.dialobot 
                else:
                    choice = random.choice([0,1])
                    agent = self.dialobot if choice==1 else self.blenderbot
                    agent_name = "Dialo" if choice==1 else "Blender"

                if agent is self.dialobot:
                    history = self.db.search(Query().user_id==user_id)[0]["dialo_history"]
                    history = await self.hist_to_tensor(history, dialo=True)
                else:
                    history = self.db.search(Query().user_id==user_id)[0]["blender_history"]
                    history = await self.hist_to_tensor(history)

                async with self.client.action(event.chat, 'typing'):
                    resp = agent.get_response(input_ids_extern=history)
                    msg = f"<i><b>{agent_name}</b></i>: {resp}"
                    await event.respond(msg, parse_mode="html")

                await self.update_blender_history(resp, user_id)
                await self.update_dialo_history(resp, user_id)

                # print histories DELETE LATER
                dialo_history = self.db.search(Query().user_id==user_id)[0]["dialo_history"]
                hist_tokens = torch.from_numpy(np.array(dialo_history))
                print("dialo hist: ",self.dialobot.tokenizer.decode(hist_tokens[:][0]))

                blender_hist = self.db.search(Query().user_id==user_id)[0]["blender_history"]
                blender_hist = await self.hist_to_tensor(blender_hist)

                print(f"blender hist: {self.blenderbot.tokenizer.decode(blender_hist['input_ids'][:][0], skip_special_tokens=True)}")

            else:
                await event.respond("to start chatting, say /start and press yes to consent")
        # check if consented

    async def new_topic_handler(self, event): 
        '''
        generate new conversation topic to start over
        '''
        user_id = event.sender_id 
        user_exists = await self.user_in_db(user_id)
        if user_exists:
            await event.respond('generating new topic..')
            
            topic  = self.davinci.generate_conversation_topic(self.generate_conversation_instruction)
            # make sure convo topic not blank or too short
            while len(topic) < 3:
                print('regenerating starter topic..')
                topic = self.davinci.generate_conversation_topic(self.generate_conversation_instruction)
            
            await event.respond(f"<b>{topic}</b>", parse_mode="html")

            retained_hist_idx_dialo, dialo_history = self.dialobot.set_initial_history(topic, return_history_idx=True)
            retained_hist_idx_blender, blender_history = self.blenderbot.set_initial_history(topic, return_history_idx=True)
            # convert history tokens to list for json
            dialo_history = dialo_history.cpu().numpy().tolist()
            # need to convert BatchEncoding obj to dict for json
            blender_hist = await self.tensor_to_hist(blender_history)

            self.db.update({'blender_history': blender_hist}, Query().user_id==user_id)
            self.db.update({'dialo_history': dialo_history}, Query().user_id==user_id)
            self.db.update({'topic': topic}, Query().user_id==user_id)
            print(self.db.search(Query().user_id==user_id))
        else: 
            await event.respond("Please consent to chatting first via /start and the yes button.")



    async def bye_handler(self, event): 
        '''
        handle /stop command, remove user from db
        '''
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        print(user_exists)
        if user_exists:
            self.db.remove(where('user_id') == user_id)
            await event.respond("you have been removed")
        else:
            await event.respond("you never even said hello")


    async def remind_handler(self, event): 
        '''
        Reminds user the original conversation topic
        '''
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        
        if user_exists: 
            topic = self.db.search(Query().user_id==user_id)[0]['topic']
            await event.respond(f"<b>{topic}</b>", parse_mode='html')
        else:
            await event.respond("You don't have a convo topic yet... go /start")

    async def pass_handler(self, event): 
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists:
            choice = random.choice([0,1])
            agent = self.dialobot if choice==1 else self.blenderbot
            agent_name = "Dialo" if choice==1 else "Blender"

            if agent is self.dialobot:
                history = self.db.search(Query().user_id==user_id)[0]["dialo_history"]
                history = await self.hist_to_tensor(history, dialo=True)
            else:
                history = self.db.search(Query().user_id==user_id)[0]["blender_history"]
                history = await self.hist_to_tensor(history)

            resp = agent.get_response(input_ids_extern=history)
            msg = f"<i><b>{agent_name}</b></i>: {resp}"
            await event.respond(msg, parse_mode="html")
            await self.update_blender_history(resp, user_id)
            await self.update_dialo_history(resp, user_id)

    def run(self):
        '''
        Add all event handlers and run client
        '''
        with self.client:
            print('started')
            self.client.add_event_handler(self.start_handler, events.NewMessage(pattern='/start'))
            self.client.add_event_handler(self.new_topic_handler, events.NewMessage(pattern='/newtopic'))
            self.client.add_event_handler(self.bye_handler, events.NewMessage(pattern='(/stop|/bye)'))
            self.client.add_event_handler(self.remind_handler, events.NewMessage(pattern='/reminder'))
            self.client.add_event_handler(self.pass_handler, events.NewMessage(pattern='/pass'))
            self.client.add_event_handler(self.yes_handler, events.CallbackQuery(data=b'yes'))
            self.client.add_event_handler(self.no_handler, events.CallbackQuery(data=b'no'))
            self.client.add_event_handler(self.response_handler, events.NewMessage(outgoing=False))
            self.client.run_until_disconnected()

if __name__=='__main__':
    username = 'Bot'
    APP_CONFIG_API_ID = os.getenv('APP_CONFIG_API_ID')
    APP_CONFIG_API_HASH = os.getenv('APP_CONFIG_API_HASH')

    multichat = MultiBotChat(username, APP_CONFIG_API_ID, APP_CONFIG_API_HASH)
    multichat.run()
