from . import config as conf
from transformers import BlenderbotTokenizer, BlenderbotForConditionalGeneration
import torch
import logging
logging.basicConfig(level=logging.INFO)


class Blenderbot:
    """
    This class contains the model and tokenizer from huggingface, 
    custom persona, and chat history
   """ 
    
    def __init__(self, mname = conf.blender_mname, name = conf.blender_name, max_position_embeddings = 128, persona=conf.blender_persona):
        logging.info("loading blender model " + mname)
        self.name = name
        self.model = BlenderbotForConditionalGeneration.from_pretrained(mname)
        self.tokenizer = BlenderbotTokenizer.from_pretrained(mname) 

        self.max_len_history = max_position_embeddings 
        self.persona = persona
        self.chat_history  = {'input_ids': torch.as_tensor([], dtype=torch.int),
                                  'attention_mask': torch.as_tensor([], dtype=torch.int)}

        self.history_clip_buffer = 20 # clips the history a bit extra to allow for new response generation
        self.retained_history_idx = 0 # only clip history after here

    def set_persona(self, new_persona):
        """
        Set new persona, conforming to persona format in config
        """
        self.persona = new_persona 

    def set_initial_history(self, convo_topic, return_history_idx=False):
        '''
        Set the history with the conversation topic and define retained_history_idx 
        '''
        persona_n_topic = self.persona + convo_topic + "\n"
        tokens = self.tokenizer([persona_n_topic], return_tensors='pt')
        
        self.retained_history_idx = tokens["input_ids"].shape[-1]
        self.chat_history = tokens 

        if return_history_idx:
            return self.retained_history_idx, self.chat_history  

    
    def update_chat_history(self, new_response): 
        """
        adds new response to chat_history 
        """
        response = self.tokenizer([new_response + "\n"], return_tensors='pt') # input_ids and attention_mask dict
        for k in response:
            self.chat_history[k] = torch.cat([self.chat_history[k], response[k]], dim=-1)

        # clip history if necessary, and save new clipped history
        if self.chat_history["input_ids"].shape[-1] >= self.max_len_history:
            # history set to clipped history and input
            self.chat_history = self.clip_history(self.chat_history)


    def clip_history(self, inputs): 
        logging.info("clipping blender history...")
        excess = inputs["input_ids"].shape[-1] - self.max_len_history + self.history_clip_buffer 
        for k in inputs: # inputs is dict consisting of input_ids and attention_mask tensors
            history_to_keep = inputs[k][:, :self.retained_history_idx]
            clipped_history = inputs[k][:, self.retained_history_idx + excess:]
            inputs[k] = torch.cat([history_to_keep, clipped_history], dim=-1) 
        
        return inputs

    def get_response(self, input_text="", input_ids_extern=None):
        """
        Generates response based on history and input_text
        --> keep input_text empty if updated history with input first
        """
        logging.info('getting blender resp--')
        # add input text to chat history, or persona and input text in first round
        if input_text:
            input_text  =  input_text + "\n" # + self.name + ": " 

            inputs = self.tokenizer([input_text], return_tensors='pt')

            # add chat history to input
            for k in inputs:
                inputs[k] = torch.cat([self.chat_history[k], inputs[k]], dim=-1)
            

            reply_ids = self.model.generate(**inputs)
        elif input_ids_extern is not None: 
            reply_ids=self.model.generate(**input_ids_extern)
        else:
            reply_ids = self.model.generate(**self.chat_history) 


        response =  self.tokenizer.batch_decode(reply_ids, skip_special_tokens=True)[0]


        return response 

        
     


if __name__ == '__main__':
    testblender = Blenderbot(name="BillyBob") 
    print(testblender.persona)
    testblender.set_initial_history("Topic: what's the point of existence?")
    for n in range(5): 
        inp = input("you: ")
        resp =  testblender.get_response(inp)
        print("Blender: ", resp)
        testblender.update_chat_history("U: " + inp)
        testblender.update_chat_history("B: " + resp)
    print("history: ", testblender.tokenizer.decode(testblender.chat_history["input_ids"][:][0], skip_special_tokens=True))

