from . import config as conf
import openai
import logging
logging.basicConfig(level=logging.INFO)


class Davinci:
    """
    This class contains history and functions for OpenAI GPT3 davinci model
    """
    
    openai.api_key = conf.OPENAI_API_KEY 

    def __init__(self, name = conf.openai_name, temperature = conf.davinci_temperature, max_tokens = conf.davinci_max_tokens, top_p = conf.davinci_top_p, frequency_penalty=conf.davinci_frequency_penalty, presence_penalty=conf.davinci_presence_penalty, prompt = conf.davinci_prompt):
        logging.info("initializing davinci..")
        self.name = name
        self.temperature = temperature
        self.max_tokens = max_tokens
        self.top_p = top_p
        self.frequency_penalty = frequency_penalty
        self.presence_penalty = presence_penalty
        self.chat_history = ""
        self.retained_history_idx = 0
        self.max_history_length = 1200
        self.prompt = prompt
        self.stops = conf.davinci_stops 

    def generate_conversation_topic(self, instruction= conf.davinci_instruct_starter): 
        '''
        uses davinci-instruct-beta to generate a conversation starter
        '''
        resp = openai.Completion.create(
            prompt = instruction,
            engine = "davinci-instruct-beta",
            temperature = self.temperature,
            max_tokens = self.max_tokens,
            top_p = self.top_p, 
            frequency_penalty = self.frequency_penalty,
            presence_penalty = self.presence_penalty,
            stop = "\n",
        )

        return resp.choices[0].text.strip()

    def direct_conv_back_to_topic(self, topic): 
        '''
        uses davinci-instruct-beta to steer back to original conversation starter
        '''
        instruction = "Give an interesting statement or question which steers the conversation back to the original topic of " + topic + ":\t"

        resp = openai.Completion.create(
            prompt = instruction,
            engine = "davinci-instruct-beta",
            temperature = self.temperature,
            max_tokens = self.max_tokens,
            top_p = self.top_p, 
            frequency_penalty = self.frequency_penalty,
            presence_penalty = self.presence_penalty,
            stop = "\n",
        )

        return resp.choices[0].text.strip()

    def set_stop_triggers(self, stops):
        self.stops = stops

    def set_initial_history(self, topic): 
        '''
        Set the history with the conversation topic and define retained_history_idx 
        '''
        prompt = self.prompt + "\nThe topic of the conversation is: " + topic + "\n"

        self.update_chat_history(prompt)

        self.retained_history_idx = len(prompt.split())

    def update_chat_history(self, new_response):
        '''
        Adds new_response to chat_history and clips history as necessary
        '''
        self.chat_history = self.chat_history + new_response + "\n"

        if len(self.chat_history.split()) > self.max_history_length:
            self.chat_history = self.clip_history(self.chat_history) 


    def clip_history(self, input_txt, n_words_to_clip=200): 
        logging.info('clipping davinci history..')
        # clip off n_words_to_clip tokens if get too long
        # this could cause problems if retained history is extremely long, but in most cases prompt won't be over max length
        words = input_txt.split()
        history_to_keep = words[:self.retained_history_idx]
        clipped_history = words[self.retained_history_idx + n_words_to_clip:]
        new_history = history_to_keep + clipped_history

        return " ".join(new_history)




    def get_response(self, input_text=""):
        '''
        Generates response based on history and input_text
        --> keep input_text empty if updated history with input first
        '''
        if input_text:
            full_input = self.chat_history + input_text + "\n"+ self.name + ":\t"
        else:
            full_input = self.chat_history + self.name + ":\t"
            

        _resp = openai.Completion.create(
            prompt = full_input,
            engine = "davinci",
            temperature = self.temperature,
            max_tokens = self.max_tokens,
            top_p = self.top_p, 
            frequency_penalty = self.frequency_penalty,
            presence_penalty = self.presence_penalty,
            stop = self.stops 
            )

        resp = _resp.choices[0].text.strip()


        return resp


if __name__=="__main__":
    testgpt = Davinci()

    topic = testgpt.generate_conversation_topic()
    print("Topic: ",topic)
    
    print("Redirect: ", testgpt.direct_conv_back_to_topic(topic))
    # add generated topic to davinci's history
    testgpt.set_initial_history(topic)

    for n in range(5):
        input_txt = input("You: ")
        input_txt = "Human: " + input_txt 
        testgpt.update_chat_history(input_txt)
        resp=testgpt.get_response()

        print("Davinci: ", resp)


    print("history: \n", testgpt.chat_history)
    
