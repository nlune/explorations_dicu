import os
from dotenv import load_dotenv
load_dotenv() 

# names for our agents
dialo_name =  "Dialo"
blender_name = "Blender"
openai_name = "Davinci" 

# settings for DialoGPT model
dialogpt_mname = "microsoft/DialoGPT-large"
dialo_max_len = 128  # max length of input sequence with chat history. dialogpt was also trained on max token len 128 
dialo_do_sample = True
dialo_top_p = 0.85 # sample from any number of most likely words once the combined probability reaches 85% 
dialo_top_k=100 # sample from this number of words w/ highest probabilities
dialo_temperature=0.75 
dialo_repetition_penalty=1.05

# settings for Blender model
blender_mname = 'facebook/blenderbot-3B'
blender_persona = "" # 'your persona: I am a theatre performer.\nyour persona: I live in Dusseldorf.\n'



# keys for GPT3, used for Davinci class
OPENAI_API_KEY = os.getenv("OPENAI_API_KEY")
davinci_temperature = 0.9
davinci_max_tokens = 150
davinci_top_p = 1
davinci_frequency_penalty = 0.8
davinci_presence_penalty = 0.5
davinci_stops = "\n" 

# used for generate_conversation_topic in Davinci class
davinci_instruct_starter = "Give an interesting, philosophical, or provocative conversation starter about theatre, feminism, or democracy:\t"
davinci_prompt = "The following is a conversation between multiple philosophers. The speaker is hilarious, contrarian, optimistic and insightful."
