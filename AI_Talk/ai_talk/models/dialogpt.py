from . import config as conf
from transformers import AutoModelForCausalLM, AutoTokenizer  
import torch
import logging
logging.basicConfig(level=logging.INFO)


class DialoGPT:
    """
    Dialogpt model from huggingface with functions for chat
    """

    def __init__(self, mname=conf.dialogpt_mname, name = conf.dialo_name, max_len_history = conf.dialo_max_len, do_sample = conf.dialo_do_sample,top_p = conf.dialo_top_p, top_k = conf.dialo_top_k, temperature = conf.dialo_temperature, repetition_penalty = conf.dialo_repetition_penalty): 

        logging.info("loading dialogpt model " + mname)
        self.name = name
        self.model = AutoModelForCausalLM.from_pretrained(mname)
        self.tokenizer = AutoTokenizer.from_pretrained(mname)

        self.max_len_history = max_len_history # maximum length of input with chat history taken by model
        self.do_sample = do_sample
        self.top_p = top_p
        self.top_k = top_k
        self.temperature = temperature
        self.repetition_penalty = repetition_penalty
        self.chat_history = torch.as_tensor([], dtype=torch.int)

        self.retained_history_idx = 0 # only clip history after here
        self.history_clip_buffer = 30 # clips the history a bit extra to allow for new response generation

    def set_initial_history(self, convo_topic: str, return_history_idx=False): 
        '''
        Set the history with the conversation topic and define retained_history_idx 
        '''
        tokens = self.tokenizer.encode(convo_topic + self.tokenizer.eos_token, return_tensors='pt')
        
        self.retained_history_idx = tokens.shape[-1]

        self.chat_history = tokens

        if return_history_idx:
            return self.retained_history_idx, self.chat_history 

        

    def update_chat_history(self, new_response): 
        """
        adds new response to chat_history 
        """
        # encode new response with End-of-String (EOS) token
        response_ids = self.tokenizer.encode( new_response + self.tokenizer.eos_token, return_tensors='pt')
        # add to chat history
        self.chat_history = torch.cat([self.chat_history, response_ids], dim=-1)

        # clip history if necessary, and save new clipped history
        if self.chat_history.shape[-1] >= self.max_len_history:
            # history set to clipped history and input
            self.chat_history = self.clip_history(self.chat_history)


    def clip_history(self, input_ids):
        logging.info('clipping dialo history...')
        history_to_keep = input_ids[:, :self.retained_history_idx]
        excess = input_ids.shape[-1] - self.max_len_history + self.history_clip_buffer 
        clipped_history = input_ids[:, self.retained_history_idx + excess:]
        # print("history to keep: ", self.tokenizer.decode(history_to_keep[:][0]))
        # print("clipped history: ", self.tokenizer.decode(clipped_history[:][0]))

        # keep only initial history and most recent history
        input_ids = torch.cat([history_to_keep, clipped_history], dim=-1)


        return input_ids 

    def get_response(self, input_text="", input_ids_extern = None):
        '''
        Generates response based on history and input_text
        --> keep input_text empty if updated history with input first
        '''
        logging.info('getting dialo response --')
        if input_text:
            # encode input w/ EOS token
            _input_ids = self.tokenizer.encode(input_text + self.tokenizer.eos_token , return_tensors='pt')
            # Append tokens from new input to chat history for generating response
            input_ids = torch.cat([self.chat_history, _input_ids], dim=-1) 
        elif input_ids_extern is not None:
            input_ids = input_ids_extern 
        else:
            input_ids = self.chat_history


        # model generates response while respecting max_length
        resp_w_history = self.model.generate(
              input_ids,
              do_sample = self.do_sample,
              max_length = self.max_len_history,
              top_p = self.top_p,
              top_k = self.top_k,
              temperature = self.temperature,
              repetition_penalty= self.repetition_penalty,
              pad_token_id = self.tokenizer.eos_token_id
              )

        # decode new portion of chat history for output
        response = self.tokenizer.decode(resp_w_history[:, input_ids.shape[-1]:][0], skip_special_tokens=True)



        return response


if __name__=='__main__':
    testdiabot=DialoGPT(name="")
    testdiabot.set_initial_history("on the topic of love")
    for n in range(5):
        inp = input("you: ")
        resp =  testdiabot.get_response(inp)
        print("Dialo: ", resp)
        testdiabot.update_chat_history(inp)
        testdiabot.update_chat_history(resp)

    print(testdiabot.tokenizer.decode(testdiabot.chat_history[:][0]))

