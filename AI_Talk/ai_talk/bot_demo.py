from telethon.tl.types import DocumentAttributeAudio, InputMediaUploadedDocument
from audio import Podcast, GCloudWavenetVoiceEngine, Gender
from telethon.sync import TelegramClient, events
from models.blenderbot import Blenderbot 
from models.openai_gpt3 import Davinci 
from models.dialogpt import DialoGPT
from dotenv import load_dotenv
from datetime import datetime
from bs4 import BeautifulSoup
import numpy as np
import logging
import random
import time
import pytz
import os
from dotenv import load_dotenv
load_dotenv() 
import ast

load_dotenv() 

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(filename)s - %(levelname)s - %(message)s",
    datefmt="%d-%b-%y %H:%M:%S"
)
APP_CONFIG_API_ID = os.getenv('APP_CONFIG_API_ID')
APP_CONFIG_API_HASH = os.getenv('APP_CONFIG_API_HASH')

username = 'anon'
# Use '"@username"' (if public) or '<channel_id>' (note: this must be an integer, without quotes; e.g. -1001595992709).
CHANNEL = ast.literal_eval(os.getenv("CHANNEL"))

# Create the client and connect
client = TelegramClient(username, APP_CONFIG_API_ID, APP_CONFIG_API_HASH) 
client.start()
logging.info("Client Created")

episode = 1
tz = pytz.timezone('Europe/Berlin')


async def talkshow_episode(topic="", show_minutes = 3, davinci_turns: int = 4): 
    '''
    Runs talkshow on telegram channel. Default davinci chosen topic, run show for show_minutes.
    '''
    blenderbot = Blenderbot()
    dialobot = DialoGPT()
    davinci_names = ["Desdemona","David","Veronica","Vince"] 
    name1 = davinci_names.pop(random.randint(0,3)) # change to random.choice(list(range(len(davinci_names)))) if using more name options
    name2 = davinci_names.pop(random.randint(0,2))
    print(name1,name2)
    davinci = Davinci(name=name1) # uses default prompt in config
    davinci2 = Davinci(name=name2, prompt = "The following is a conversation between multiple entertainers. The speaker is hilarious, witty, sarcastic and insightful." ) 

    agents = [blenderbot, dialobot, davinci, davinci2]
    stop_triggers = [f"{a.name}:" for a in agents]
    stop_triggers.insert(0,"\n")

    engine = GCloudWavenetVoiceEngine
    moderator = engine.get_voice()
    podcast = Podcast(
        moderator,
        speakers = [
            engine.get_voice(blenderbot.name, Gender.MALE),
            engine.get_voice(dialobot.name, Gender.MALE),
            engine.get_voice(davinci.name, Gender.FEMALE),
            engine.get_voice(davinci2.name, Gender.FEMALE),
        ]
    )

    # stop triggers for davinci models to only contain other agents' names
    stop_triggers_d1 = stop_triggers[:3] + stop_triggers[-1:]
    stop_triggers_d2 = stop_triggers[:4]

    # set stop trigger words for Davinci models
    davinci.set_stop_triggers(stop_triggers_d1)
    davinci2.set_stop_triggers(stop_triggers_d2)

    now = datetime.now(tz)
    # timeframe = 'afternoon' if now.hour < 17 else 'evening'

    # Complete conversation history for this podcast (useful for TTS).
    history = list()

    text = "<i>Welcome to AI Love Talk <3 The following is a conversation between several transformer models from Hugging Face and OpenAI. Davinci will be deciding on the relevant conversation topic. The show will run for 3 minutes twice daily, so stay tuned! Let's see what happens.</i>\n"
    await client.send_message(CHANNEL, text, parse_mode="html")
    history.append((moderator.name, text))

    if topic:
        input_txt = topic
    else:
        input_txt = davinci.generate_conversation_topic()
        topic = input_txt # used for redirection

    # make sure convo topic not blank or too short
    while len(input_txt) < 5:
        logging.info('regenerating starter topic..')
        input_txt = davinci.generate_conversation_topic()


    n_agents = len(agents)
    agents_idx = list(range(n_agents))
    # pick initial speaker, start w/ davinci model
    speaker_idx = random.choice([2,3])

    text = f"<b>Conversation starter: {input_txt}</b>"
    await client.send_message(CHANNEL, text, parse_mode="html")
    history.append((moderator.name, f"The topic for today's podcast is \"{input_txt}\""))

    n = 0 
    start_time = time.time()
    # keep running loop while current time minus start time less than show_minutes
    while (time.time() - start_time) < (60*show_minutes):
        curr_agent  = agents[speaker_idx]

        # update histories of agents or set initial history
        for a in agents:
            if n==0: 
                a.set_initial_history(input_txt)

            else:
                if a is davinci or a is davinci2:
                    a.update_chat_history(davinci_input)
                else:
                    a.update_chat_history(input_txt)

        # # davinci redirect code 
        # if (n+1)%redirect_every_x_turns==0:
        #     print('redirecting convo...') 
        #     curr_agent = redirect_bob 
        #     resp = curr_agent.direct_conv_back_to_topic(topic)
        # else:
        #     resp = curr_agent.get_response()

        n+=1
        gen_start=time.time()

        resp = curr_agent.get_response()

        try_for_nonempty_resp = 0
        while len(resp) < 3 and try_for_nonempty_resp < 7:
            logging.info('try for nonempty resp..')
            resp = curr_agent.get_response(input_txt)
            try_for_nonempty_resp+=1

        gen_end=time.time()

        total_elapsed = gen_end - gen_start 
        expected_time = len(resp)//10 # assuming typing speed of 10 char per sec

        diff=expected_time-total_elapsed
        # wait a bit if elapsed time less than expected time
        if diff > 0:
            time.sleep(diff) 

        message = f"<b>{curr_agent.name}:</b> {resp}"
        await client.send_message(CHANNEL, message, parse_mode="html")
        # Adding it to the total history for the podcast
        history.append((curr_agent.name, resp))

        # start with just davinci models chatting
        # or choose next speaker randomly
        if n < davinci_turns:
            speaker_idx = 3 if speaker_idx==2 else 2
        else:
            speaker_idx = random.choice([i for i in agents_idx if i != speaker_idx])

        input_txt = resp
        davinci_input = curr_agent.name + ": " + resp

    
    logging.info('\ndavinci history \n' + davinci.chat_history + '\n')
    logging.info('blender history \n' + blenderbot.tokenizer.decode(blenderbot.chat_history["input_ids"][:][0], skip_special_tokens=True) + '\n')
    logging.info('dialo history \n' + dialobot.tokenizer.decode(dialobot.chat_history[:][0]))

    logging.info('num turns: %s', n)

    message = f"<i>Thanks for {{word}}! Tune in again next time for another episode of AI Love Talk <3</i>"
    history.append((moderator.name, message.format(word="listening")))
    message = message.format(word="watching")

    pfile, duration = await podcast.podcast(history)
    pfile.name = f"AI Podcast - Episode #{episode}.mp3"

    file_obj = await client.upload_file(pfile)
    media = InputMediaUploadedDocument(
        mime_type = "audio/mp3",
        file = file_obj,
        attributes = [DocumentAttributeAudio(
            duration=round(duration / 1000),  # Duration is in milliseconds.
            voice=False,
            title=f"Episode #{episode}",
            performer="AI Podcast",
            waveform=None,
        )]
    )
    await client.send_file(CHANNEL, media, caption=message, parse_mode="html", allow_cache=False)
    # await client.send_message(CHANNEL, message, parse_mode="html")


if __name__ == "__main__":
    now = datetime.now(tz)
    # no need for while loop as we run the script once on reboot
    minute = str(now.minute) if len(str(now.minute))==2 else "0"+str(now.minute) 
    # print("checking if time to air at hour " + str(now.hour))
    logging.info(f"airing episode at {now.hour}:{minute}")
    with client:
        client.loop.run_until_complete(talkshow_episode())
    # while True:
    #     now = datetime.now(tz)
    #     # print("checking if time to air at hour " + str(now.hour))
    #     if True:  # now.hour in (12, 18):
    #         logging.info("airing episode %s", episode)
    #         with client:
    #             client.loop.run_until_complete(talkshow_episode())
    #     episode += 1
    #     logging.info("waiting...")
    #     # check again in 1 hour
    #     time.sleep(3600)
