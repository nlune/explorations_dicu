from .base import Preset, Gender
from .podcast import Podcast

from .gcloud import GCloudStandardVoiceEngine, GCloudWavenetVoiceEngine
from .gtts import GTTSVoiceEngine

__all__ = (
    "Preset",
    "Gender",
    "Podcast",
    "GCloudStandardVoiceEngine",
    "GCloudWavenetVoiceEngine",
    "GTTSVoiceEngine",
)
