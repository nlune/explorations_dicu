from pydub import AudioSegment
from bs4 import BeautifulSoup
from io import BytesIO
from gtts import gTTS
import asyncio

from .base import BaseVoiceEngine, Preset, Gender


class GTTSVoiceEngine(BaseVoiceEngine):
    # Here we apply a hack for gTTS to get different voices: using locales of Google.
    DEFAULT_SPEAKERS_PRESETS = [
        Preset("Chloe", Gender.FEMALE, "com"),
        Preset("Mia", Gender.FEMALE, "com.au"),  # Australian accent
        Preset("Emma", Gender.FEMALE, "ca"),  # Canada
        Preset("Avni", Gender.FEMALE, "co.in"),  # India
        Preset("Fiona", Gender.FEMALE, "ie"),  # Ireland
    ]

    def __init__(self, preset: Preset):
        self.language = "en"
        self.gender = preset.gender

        self.name = preset.name
        self.preset = preset.value

    async def say(self, text: str):
        def _text_to_speech(text: str, lang: str, tld: str):
            mp3 = BytesIO()
            # Text with html tags causes issues.
            text = BeautifulSoup(text, "lxml").text
            # Doing an API call and writing to the in-memory file.
            gTTS(text, tld=tld, lang=lang).write_to_fp(mp3)
            mp3.seek(0)
            return mp3
            #
            # ogg = BytesIO()
            # Convert from .mp3 to an .ogg with 'libopus' codec (telegram will recognize the file better).
            # seg = AudioSegment.from_file(mp3, format="mp3")
            # seg.export(ogg, format="ogg", codec="libopus")
            # ogg.seek(0)
            # return ogg

        return await asyncio.get_event_loop().run_in_executor(
            None, _text_to_speech, text, self.language, self.preset,
        )
