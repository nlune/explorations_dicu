from google.cloud import texttospeech
from bs4 import BeautifulSoup
from io import BytesIO
import asyncio
import random
import os

from .base import BaseVoiceEngine, Preset, Gender


__all__ = (
    "GCloudStandardVoiceEngine",
    "GCloudWavenetVoiceEngine",
)

class GCloudBaseVoiceEngine(BaseVoiceEngine):
    """This base class implements Google Cloud Text To Speech API"""
    def __init__(self, preset: Preset):
        self.language = "en"
        self.gender = preset.gender

        self.name = preset.name
        self.preset = preset.value

        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "voice_credentials.json"
        if not os.path.exists(os.environ["GOOGLE_APPLICATION_CREDENTIALS"]):
            raise FileNotFoundError(
                f"Could not find credentials file '{os.environ['GOOGLE_APPLICATION_CREDENTIALS']}'!"
            )

        # Instantiates a text-to-speech client.
        self.tts_client = texttospeech.TextToSpeechClient()
        # Select the type of audio file you want returned (may try returning popular OGG OPUS mode, used by telegram).
        self.audio_config = texttospeech.AudioConfig(
             # audio_encoding=texttospeech.AudioEncoding.OGG_OPUS
             audio_encoding=texttospeech.AudioEncoding.MP3
        )

    async def say(self, text: str):
        def _text_to_speech(
            text: str, lang: str, preset: str,
            client: texttospeech.TextToSpeechClient,
            audio_config: texttospeech.AudioConfig,
        ):
            # Build the voice request, select the language code ("en-US").
            voice = texttospeech.VoiceSelectionParams(language_code=lang, name=preset)

            # Text with html tags causes issues.
            text = BeautifulSoup(text, "lxml").text
            # Set the text input to be synthesized
            synthesis_input = texttospeech.SynthesisInput(text=text)

            # Perform the text-to-speech request on the text input with the selected voice parameters and audio file type.
            response = client.synthesize_speech(
                input=synthesis_input, voice=voice, audio_config=audio_config
            )

            # The response's audio_content is binary.
            ogg = BytesIO()
            ogg.write(response.audio_content)
            ogg.seek(0)
            return ogg

        return await asyncio.get_event_loop().run_in_executor(
            None, _text_to_speech, text, self.language, self.preset, self.tts_client, self.audio_config,
        )


# TODO: Find good voices here (though they are all bad, so not sure it's worth it).
class GCloudStandardVoiceEngine(GCloudBaseVoiceEngine):
    pass


class GCloudWavenetVoiceEngine(GCloudBaseVoiceEngine):
    DEFAULT_SPEAKERS_PRESETS = [
        # Australian accents
        Preset("Rose", Gender.FEMALE, "en-AU-Wavenet-A"),
        Preset("Jonas", Gender.MALE, "en-AU-Wavenet-B"),
        Preset("Hermione", Gender.FEMALE, "en-AU-Wavenet-C"),
        Preset("Kyle", Gender.MALE, "en-AU-Wavenet-D"),

        # Indian accents
        Preset("Diana", Gender.FEMALE, "en-IN-Wavenet-A"),
        Preset("Denis", Gender.MALE, "en-IN-Wavenet-B"),
        Preset("Isaac", Gender.MALE, "en-IN-Wavenet-C"),
        Preset("Elizabeth", Gender.FEMALE, "en-IN-Wavenet-D"),

        # Great Britain accents.
        Preset("Laura", Gender.FEMALE, "en-GB-Wavenet-A"),
        Preset("Bob", Gender.MALE, "en-GB-Wavenet-B"),
        Preset("Jannet", Gender.FEMALE, "en-GB-Wavenet-C"),
        Preset("Lewis", Gender.MALE, "en-GB-Wavenet-D"),
        Preset("Viktoria", Gender.FEMALE, "en-GB-Wavenet-F"),

        # USA accents.
        Preset("Chris", Gender.MALE, "en-US-Wavenet-A"),
        Preset("Dunkan", Gender.MALE, "en-US-Wavenet-B"),
        Preset("Nayomi", Gender.FEMALE, "en-US-Wavenet-C"),
        Preset("Henry", Gender.MALE, "en-US-Wavenet-D"),
        Preset("Nadyah", Gender.FEMALE, "en-US-Wavenet-E"),
        Preset("Valerie", Gender.FEMALE, "en-US-Wavenet-F"),
        # @Note: skipping 'en-US-Wavenet-H', it's too similar to 'F'.
        Preset("Victor", Gender.MALE, "en-US-Wavenet-I"),
        Preset("Andrew", Gender.MALE, "en-US-Wavenet-J"),
    ]
