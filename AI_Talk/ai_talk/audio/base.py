from typing import Iterable, List, Any
from collections import namedtuple
from random import choice
from copy import deepcopy
from enum import Enum


class Gender(Enum):
    MALE    = 0
    FEMALE  = 1
    NEUTRAL = 2


class Preset:
    def __init__(self, name: str, gender: Gender, value: Any):
        self.name = name
        self.gender = gender
        self.value = value


class BaseVoiceEngine:
    DEFAULT_SPEAKERS_PRESETS: List[Preset] = []

    async def say(self, text: str):
        raise NotImplementedError("You must implement base voice 'say' method!")

    @classmethod
    def get_voice(cls, name: str = None, gender: Gender = None) -> "BaseVoiceEngine":
        assert cls.DEFAULT_SPEAKERS_PRESETS

        presets = cls.DEFAULT_SPEAKERS_PRESETS
        if gender:
            presets = list(filter(lambda p: p.gender == gender, presets))

        new_preset = choice(presets)
        cls.DEFAULT_SPEAKERS_PRESETS.remove(new_preset)

        if name:
            new_preset.name = name
        return cls(new_preset)

    @classmethod
    def iter_voices(cls) -> Iterable["BaseVoiceEngine"]:
        while cls.DEFAULT_SPEAKERS_PRESETS:
            yield cls.get_voice()

    @classmethod
    def get_voices(cls) -> List["BaseVoiceEngine"]:
        return list(cls.iter_voices())
