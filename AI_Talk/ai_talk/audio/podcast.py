from typing import List, Tuple, Any
from collections import namedtuple
from pydub.playback import play
from pydub import AudioSegment
from bs4 import BeautifulSoup
from io import BytesIO
from gtts import gTTS
import asyncio
import logging
import random


class Podcast:
    def __init__(
        self,
        moderator,
        speakers,
    ):
        self.moderator = moderator
        self.speakers = speakers

        # Now we can create a map for name and voice, which we will need later.
        self.voices = {v.name: v for v in self.speakers}
        # Add a voice for the moderator.
        self.voices[self.moderator.name] = self.moderator

        logging.info("Generated voices map: %s", self.voices)

    async def podcast(self, history: List[Tuple[str, Any]]) -> Tuple[BytesIO, int]:
        """
        Accepts history in the form of list of tuples, where 0th element is speaker's
        name (unique identifier) and 1st element is their text. And generates a podcast from it.
        NoneType instead of name means that it's a comment from the moderator.
        """
        # Await a bunch of parallel tasks for each entry.
        tasks = list()
        for entry in history:
            assert entry[0] in self.voices

            voice = self.voices[entry[0]]
            tasks.append(voice.say(entry[1]))

        files = await asyncio.gather(*tasks)

        # Combining all files into single huge audio file.
        podcast = BytesIO()
        # Adding two 'AudioSegment' objects concatinates them into one audio.
        segment = sum(AudioSegment.from_file(f, format="mp3") for f in files)
        segment.export(podcast, format="mp3")
        podcast.seek(0)

        return podcast, len(segment)
