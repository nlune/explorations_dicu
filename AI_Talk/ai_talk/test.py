from audio import GCloudWavenetVoiceEngine, GTTSVoiceEngine, Podcast
from pydub import AudioSegment
import asyncio


if __name__ == "__main__":
    # m = GTTSVoiceEngine.get_voice()
    m = GCloudWavenetVoiceEngine.get_voice()
    # vs = GTTSVoiceEngine.get_voices()
    vs = GCloudWavenetVoiceEngine.get_voices()
    p = Podcast(m, vs)
    f, length = asyncio.run(p.podcast(
        [
            (m.name, "Today we are going to talk about sport.."),
            # Note: 'get_voices' voices are already random, so no need to shuffle them here.
            (vs[1].name, "Hello, my name is Kateryna and I love to play tennis!"),
            (vs[3].name, "Hmm, hi Kateryna! How long have you been playing tennis for?"),
            (vs[1].name, "I forgot. Probably like 10 or 11 years already."),
            (vs[3].name, "Wow.. Well, you are going to beat me."),
            (vs[0].name, "I want to play some tennis too with you guys."),
            (vs[2].name, "Hi, are you guys talking about table tennis, cuz i'ma fan."),
            (vs[1].name, "Not really, no, We were talking about big tennis."),
            (vs[2].name, "Ehh, unfortunately I can't play that. My rocket broke few month ago.."),
            (vs[4].name, "Football, anyone?.."),
            (vs[5].name, "Yes! I love football. Prepare to be crushed, haha.."),
            (vs[4].name, "Wait, no. I meant soccer."),
        ]
    ))
    AudioSegment.from_file(f, format="ogg").export("/tmp/tmpsound.mp3", format="mp3")
