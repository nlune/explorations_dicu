from models.blenderbot import Blenderbot 
from models.dialogpt import DialoGPT
from models.openai_gpt3 import Davinci 
import random
import torch
import time

def two_bot_chat(blender, dialogpt, prompt, n_rounds=5, interject_every_x_turns=3):
    '''
    chat between an instance of Blenderbot and DialoGPT for n_rounds
    give a prompt to get the conversation started
    user input taken interject_every_x_turns 
    **DEPRECATED **
    '''
    input_txt = prompt
    turn = 1
    for n in range(n_rounds):
        if n==0:
            print("Prompt: ", input_txt) 
        if (n+1)%(interject_every_x_turns)==0:
            input_txt = input("you: ")
            # randomly choose next speaker
            turn = random.choice([0,1])

            # add history for the one who isn't next speaker
            if turn == 1:
                # add history for blender
                blender.update_chat_history(input_txt)
            else:
                # add history for dialo
                dialogpt.update_chat_history(input_txt)

        if turn == 0:
            input_txt = blender.get_response(input_txt)
            print("Blender: ", input_txt)
            turn = 1 
        elif turn == 1:
            input_txt = dialogpt.get_response(input_txt)
            print("Dialo: ", input_txt)
            turn = 0


    
def three_bot_chat(n_min:int = 3, user_prompt: bool = False ):
    '''
    Rehearsal for the talkshow channel
    '''
    blenderbot = Blenderbot()
    dialobot = DialoGPT()
    davinci = Davinci()
    agents = [blenderbot, dialobot, davinci]

    if user_prompt:
        input_txt = input("Enter the conversation topic: ")
    else:
        input_txt = davinci.generate_conversation_topic()
        # ensure topic not empty 
        while len(input_txt) < 3:
            input_txt = davinci.generate_conversation_topic()
        print(input_txt)

    n_agents = len(agents)
    agents_idx = [i for i in range(n_agents)]

    # turn_order =random.choices(
    #         population= agents_idx,
    #         weights= probability_speaker, 
    #         k= n_rounds
    # )

    # print(turn_order)

    
    # pick initial speaker
    speaker_idx = random.choice([i for i in range(n_agents)])

    # talk for n_min
    # TODO: change to time calculated based on responses length
    n=0
    start_time = time.time() 
    while (time.time() - start_time) < (60*n_min): 
        curr_agent  = agents[speaker_idx]

        # update histories of other agents or set initial history
        for a in agents:
            if n==0: 
                a.set_initial_history(input_txt)
            else:
                a.update_chat_history(input_txt)
        n+=1
        gen_start=time.time()
        _resp = curr_agent.get_response()
        gen_end=time.time()

        total_elapsed = gen_end - gen_start 
        expected_time = len(_resp)//10 # assuming typing speed of 10 char per sec
        #print(f'total elapsed: {total_elapsed}, len resp {len(_resp)}, expected time: {expected_time}') 
        
        diff=expected_time-total_elapsed

        # wait a bit if elapsed time less than expected time
        if diff>0:
            time.sleep(diff) 

        
        resp = curr_agent.name + ": " + _resp 
        print(resp)

        # choose next speaker
        speaker_idx = random.choice([i for i in agents_idx if i != speaker_idx])
        # set response as input for next round
        input_txt = resp


    print('\ndavinci history \n' + davinci.chat_history + '\n')
    print('blender history \n' + blenderbot.tokenizer.decode(blenderbot.chat_history["input_ids"][:][0], skip_special_tokens=True) + '\n')
    print('dialo history \n' + dialobot.tokenizer.decode(dialobot.chat_history[:][0]))
    print("length: ", len(dialobot.chat_history[:][0]) )






if __name__=="__main__":
    ## debugging stop triggers
    # stops = [a.name for a in agents]
    # stops.insert(0, "\n")
    # stops.append("human:")
    # stops.pop(1)
    # print(stops)
    # davinci.set_stop_triggers(stops)

    three_bot_chat()


